﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CFW;

namespace GGJ2019.Services
{
    [System.Serializable]
    public struct SaveData
    {
        public List<int> FishIds;
    }

    public class SaveService : SingletonMonoBehaviour<SaveService>
    {
        private const string SaveDataKey = "savedata";

        public SaveData SaveData { get; private set; }

        public void Reset()
        {
            PlayerPrefs.DeleteAll();
            this.Load();
        }

        public void Load()
        {
            var json = PlayerPrefs.GetString(SaveService.SaveDataKey, "{}");
            if (string.IsNullOrEmpty(json))
            {
                return;
            }
            this.SaveData = JsonUtility.FromJson<SaveData>(json);
        }

        public void Save()
        {
            var json = JsonUtility.ToJson(this.SaveData);
            PlayerPrefs.SetString(SaveService.SaveDataKey, json);
            PlayerPrefs.Save();
        }
    }
}
