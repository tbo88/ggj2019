﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CFW;

namespace GGJ2019.Services
{
    public class BookService : SingletonMonoBehaviour<BookService>
    {
        public void Add(int fishId)
        {
            var saveData = SaveService.Instance.SaveData;
            if (saveData.FishIds.Exists(i => i == fishId))
            {
                return;
            }
            saveData.FishIds.Add(fishId);
            SaveService.Instance.Save();
        }
    }
}
