﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using CFW;

namespace GGJ2019.Services
{
    public class DataProvider : SingletonMonoBehaviour<DataProvider>
    {
        [System.Serializable]
        public class KeySpritePair
        {
            public string Key;
            public Sprite Sprite;
            public GameObject Prefab;
        }

        [SerializeField]
        private TextAsset csv;

        [SerializeField]
        private KeySpritePair[] sprites;

        private List<Fish> fishes = new List<Fish>();

        private void Start()
        {
            var reader = new CSVReader();
            var rows = reader.Read(this.csv.text);
            foreach (var i in rows)
            {
                this.fishes.Add(new Fish(i));
            }
        }

        public Fish GetFish(int fishId)
        {
            return this.fishes.Find(i => i.FishId == fishId);
        }

        public List<Fish> GetFishesByRarity(int rarity)
        {
            return this.fishes.FindAll(i => i.Rarity == rarity);
        }

        public Sprite GetIcon(string key)
        {
            var value = System.Array.Find(this.sprites, i => i.Key == key);
            return value.Sprite;
        }

        public GameObject GetPrefab(string key)
        {
            var value = System.Array.Find(this.sprites, i => i.Key == key);
            return value.Prefab;
        }
    }
}
