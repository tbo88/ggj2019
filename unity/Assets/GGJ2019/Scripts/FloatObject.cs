﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatObject : MonoBehaviour {
	[SerializeField]
	private Rigidbody rigidbody;
	void Update () {
		var position = this.gameObject.transform.position;
		if(position.y < 0)
		{
			this.rigidbody.velocity = Vector3.zero;
			this.gameObject.transform.position = new Vector3(
				position.x,
				0,
				position.z
			);
		}
	}
}
