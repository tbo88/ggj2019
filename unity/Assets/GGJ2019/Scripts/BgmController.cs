﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CFW;

namespace GGJ2019
{
	public class BgmController : SingletonMonoBehaviour<BgmController> {
		[SerializeField]
		private AudioClip[] bgms;
		[SerializeField]
		private AudioSource audio;

		public void SetBgm(int index)
		{
			if(this.bgms.Length <= index)
			{
				return;
			}
			this.audio.clip = this.bgms[index];
			this.audio.Play();
		}
	}
}