﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CFW;

namespace GGJ2019.Scenes.Game
{
    public class Game : SceneBase
    {
        public class Context : SceneContext
        {
            // nop
        }

        private const string STATE_KEY = "State";

        [SerializeField]
        private Animator animator;

        public void OnMenuClicked()
        {
            SceneService.Instance.Add(new Book.Book.Context());
        }

        public void OnEasyButtonClicked()
        {
            ScoreService.Instance.Initialize(Difficulty.Easy);
            this.animator.SetInteger(Game.STATE_KEY, 1);
        }

        public void OnNormalButtonClicked()
        {
            ScoreService.Instance.Initialize(Difficulty.Normal);
            this.animator.SetInteger(Game.STATE_KEY, 1);
        }

        public void OnHardButtonClicked()
        {
            ScoreService.Instance.Initialize(Difficulty.Hard);
            this.animator.SetInteger(Game.STATE_KEY, 1);
        }

        public void OnResetButtonClicked()
        {
            Services.SaveService.Instance.Reset();
        }
    }
}
