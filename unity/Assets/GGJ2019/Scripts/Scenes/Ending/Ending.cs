﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CFW;

namespace GGJ2019.Scenes.Ending
{
    public class Ending : SceneBase
    {
        [SerializeField]
        private Transform staffRoll;

        private bool finished;

        public class Context : SceneContext
        {
            // nop
        }

        private void Update()
        {
            if (this.finished)
            {
                return;
            }
            var pos = this.staffRoll.localPosition;
            pos.y += 40 * Time.deltaTime;
            this.staffRoll.localPosition = pos;
            if (pos.y < 576)
            {
                return;
            }
            this.FinishScroll();
        }

        private void FinishScroll()
        {
            this.finished = true;
            UnityEngine.SceneManagement.SceneManager.LoadScene("Bootstrap");
        }

        public void OnClickSkipButton()
        {
            this.FinishScroll();
        }
    }
}
