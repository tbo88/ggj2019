﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CFW;

namespace GGJ2019.Scenes.Book
{
    public class Book : SceneBase
    {
        public class Context : SceneContext
        {
            // nop
        }

        [SerializeField]
        private UnityEngine.UI.Image iconImage;

        [SerializeField]
        private UnityEngine.UI.Text nameText;

        [SerializeField]
        private UnityEngine.UI.Text descriptionText;

        [SerializeField]
        private Transform content;

        [SerializeField]
        private Icon iconPrefab;

        public override IEnumerator OnSceneLoaded(SceneContext context)
        {
            foreach (var i in this.content.GetComponentsInChildren<Icon>())
            {
                GameObject.Destroy(i.gameObject);
            }
            var fishids = Services.SaveService.Instance.SaveData.FishIds;
            foreach (var i in fishids)
            {
                var fish = Services.DataProvider.Instance.GetFish(i);

                var icon = GameObject.Instantiate(this.iconPrefab, this.content);
                icon.Clicked += this.OnIconClicked;
                icon.Initialize(fish);

                var sprite = Services.DataProvider.Instance.GetIcon(fish.ImageId);
                icon.SetSprite(sprite);
            }
            this.nameText.text = "";
            this.descriptionText.text = "";
            yield break;
        }

        public void OnCloseButtonClicked()
        {
            SceneService.Instance.Remove(this);
        }

        private void OnIconClicked(int fishId)
        {
            var fish = Services.DataProvider.Instance.GetFish(fishId);
            this.iconImage.sprite = Services.DataProvider.Instance.GetIcon(fish.ImageId);
            this.nameText.text = fish.Name;
            this.descriptionText.text = fish.Description;
        }
    }
}
