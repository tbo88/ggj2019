﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGJ2019.Scenes.Book
{
    public class Icon : MonoBehaviour
    {
        public event System.Action<int> Clicked = _ => { };

        [SerializeField]
        private UnityEngine.UI.Image image;

        private int fishId;

        public void Initialize(Fish fish)
        {
            this.fishId = fish.FishId;
        }

        public void SetSprite(Sprite sprite)
        {
            this.image.sprite = sprite;
        }

        public void OnClicked()
        {
            this.Clicked.Invoke(this.fishId);
        }
    }
}
