﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

namespace GGJ2019
{
	public class CameraService : MonoBehaviour
	{
		[SerializeField]
		private CinemachineVirtualCamera vcam1;

		[SerializeField]
		private CinemachineVirtualCamera vcam2;

		public void ChangeCamera()
		{
			var isActive = this.vcam2.gameObject.activeInHierarchy;
			this.vcam2.gameObject.SetActive(!isActive);
		}
	}
}
