﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGJ2019
{
	public class SkyRotater : MonoBehaviour
	{
		[SerializeField]
		private Transform transform;

		private void Update()
		{
			this.transform.Rotate(Vector3.forward, -Time.deltaTime);
		}
	}
}
