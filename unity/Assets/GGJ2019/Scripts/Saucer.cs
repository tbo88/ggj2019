﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGJ2019
{
	public class Saucer : MonoBehaviour
	{
		private void OnCollisionEnter(Collision obj)
		{
			ScoreService.Instance.AddScore(-100);
			GameObject.Destroy(obj.gameObject);
		}
	}
}
