﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGJ2019
{
	public class Fisher:MonoBehaviour
    {
		[SerializeField]
		private Transform fisher;
		[SerializeField]
		private GameObject floatObject;

		public void StartFishing()
		{
			this.floatObject.transform.localPosition = new Vector3(0, 0, 0);
            Rigidbody rid = this.floatObject.GetComponent<Rigidbody>();
			rid.velocity = Vector3.zero;
            rid.AddForce(new Vector3(0, 10, 10) * rid.mass, ForceMode.Impulse);
		}

		public void PullFloat()
		{
            Vector3 velocity = Calculater.CalculateVelocity(
				new Vector3(this.floatObject.transform.position.x, this.floatObject.transform.position.y, this.floatObject.transform.position.z ),
				new Vector3(this.fisher.position.x, this.fisher.position.y, this.fisher.position.z),
				45f
            );
            Rigidbody rid = this.floatObject.GetComponent<Rigidbody>();
			rid.velocity = Vector3.zero;
            rid.AddForce(velocity * rid.mass, ForceMode.Impulse);
		}
	}
}
