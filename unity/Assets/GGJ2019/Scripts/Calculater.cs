﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGJ2019
{
	static public class Calculater {
        static public Vector3 CalculateVelocity(Vector3 startPosition, Vector3 targetPosition, float angle)
        {
            float rad = angle * Mathf.PI / 180;
            float x = Vector2.Distance(new Vector2(startPosition.x, startPosition.z), new Vector2(targetPosition.x, targetPosition.z));
            float y = startPosition.y - targetPosition.y;
            float speed = Mathf.Sqrt(-Physics.gravity.y * Mathf.Pow(x, 2) / (2 * Mathf.Pow(Mathf.Cos(rad), 2) * (x * Mathf.Tan(rad) + y)));

            if (float.IsNaN(speed))
            {
                return Vector3.zero;
            }
            else
            {
                return (new Vector3(targetPosition.x - startPosition.x, x * Mathf.Tan(rad), targetPosition.z - startPosition.z).normalized * speed);
            }
        }
	}
}
