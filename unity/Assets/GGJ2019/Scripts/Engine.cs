﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CFW;

namespace GGJ2019
{
    public class Engine : MonoBehaviour
    {
        private float elapsed;
        private bool wait;

        private void Start()
        {
            this.Initialize();
        }

        private void Update()
        {
            if (!this.wait)
            {
                this.WaitHit();
            }
        }

        private void Initialize()
        {
            Random.InitState(System.DateTime.Now.Millisecond);
            Services.SaveService.Instance.Load();
            SceneService.Instance.Move(new Scenes.Game.Game.Context());
        }

        private void WaitHit()
        {
            this.elapsed += Time.deltaTime;
            if (this.elapsed < 1)
            {
                return;
            }
            this.elapsed -= 1;
            if (Random.Range(0, 10) < 5)
            {
                return;
            }
        }

        public void Through()
        {
            this.wait = !this.wait;
        }
    }
}
