﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGJ2019
{
    [System.Serializable]
    public class Fish
    {
        public int FishId;
        public string Name;
        public string ImageId;
        public int Rarity;
        public string Description;

        public Fish(Dictionary<string, object> raw)
        {
            this.FishId = int.Parse(raw["fish_id"].ToString());
            this.Name = raw["name"].ToString();
            this.ImageId = raw["image_id"].ToString();
            this.Rarity = int.Parse(raw["rarity"].ToString());
            this.Description = raw["description"].ToString();
        }
    }
}
