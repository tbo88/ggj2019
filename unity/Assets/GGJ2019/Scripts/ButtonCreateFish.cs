﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GGJ2019
{
	public class ButtonCreateFish : MonoBehaviour
	{
		[SerializeField]
		private Transform parentCanvasObject;
		[SerializeField]
		private Slider PowerGuage;
		[SerializeField]
		private Transform normalPrefab;
		[SerializeField]
		private Transform goodPrefab;
		[SerializeField]
		private Transform greatPrefab;
		[SerializeField]
		private Transform perfectPrefab;
		[SerializeField]
		private Fisher fisher;
		[SerializeField]
		private GameObject feverGuage;
		[SerializeField]
		private GameObject fishingButton;
        [SerializeField]
        private GameObject exclamation;
        [SerializeField]
        private float extension = 1f;
        [SerializeField]
        private float fishHitTimeMin = 1f;
        [SerializeField]
        private float fishHitTimeMax = 30f;

        private System.Random rand = new System.Random();
		private float power = 0f;
		private float velocity = 0.0f;
		private float waitingTime = 0f;
		private float fishHitTime = 0f;
		private bool isWait;
		private bool isUpGuage = true;
		public bool isCharge;
		private bool fever;
		private FishSpawner.Evaluation evaluation;

		private void Start()
		{
            this.exclamation.SetActive(false);
			this.fishingButton.SetActive(false);
		}

		private void Update()
		{
			if (this.isCharge)
			{
				this.ChargePower();
			}
			if(!this.isWait)
			{
				return;
			}

			this.waitingTime += Time.deltaTime;
			if(this.waitingTime > this.fishHitTime + this.extension)
			{
				this.ResetWait();
			}
			else if(this.waitingTime > this.fishHitTime)
			{
				this.Hit();
			}
		}

		private void ChargePower()
		{
			if (this.isUpGuage)
			{
				this.power = Mathf.SmoothDamp(this.PowerGuage.value, 1f, ref this.velocity, 0.5f, 3f);
			}
			else
			{
				this.power = Mathf.SmoothDamp(this.PowerGuage.value, 0f, ref this.velocity, 0.5f, 3f);
			}
			this.PowerGuage.value = this.power;
			if (this.power >= 0.999f)
			{
				this.isUpGuage = false;
			}
			else if (this.power <= 0.001f)
			{
				this.isUpGuage = true;
			}
		}

		public void OnButtonUp()
		{
            if(this.isWait)
            {
                return;
            }
			this.Fishing();
			this.isCharge = false;
			this.PowerGuage.value = 0;
			this.power = 0;
			this.isUpGuage = true;
			if(this.fever)
			{
				return;
			}
			this.fisher.StartFishing();
        }

        public void OnButtonDown()
		{
            if(this.isWait)
            {
                return;
            }
			this.isCharge = true;
		}

		private void Fishing()
		{
			if (this.fever)
			{
				GameObject.Instantiate(this.perfectPrefab, parentCanvasObject.transform);
				ScoreService.Instance.AddScore(100);
				this.evaluation = FishSpawner.Evaluation.Perfect;
				this.exclamation.SetActive(true);
				this.fishingButton.SetActive(true);
				return;
			}
			if (this.PowerGuage.value > 0.75 || this.PowerGuage.value < 0.25)
			{
				GameObject.Instantiate(this.normalPrefab, parentCanvasObject.transform);
				this.evaluation = FishSpawner.Evaluation.Normal;
			}
			else if (this.PowerGuage.value > 0.6 || this.PowerGuage.value < 0.4)
			{
				GameObject.Instantiate(this.goodPrefab, parentCanvasObject.transform);
				this.evaluation = FishSpawner.Evaluation.Good;
			}
			else if (this.PowerGuage.value > 0.525 || this.PowerGuage.value < 0.475)
			{
				GameObject.Instantiate(this.greatPrefab, parentCanvasObject.transform);
				this.evaluation = FishSpawner.Evaluation.Great;
			}
			else
			{
				GameObject.Instantiate(this.perfectPrefab, parentCanvasObject.transform);
				this.evaluation = FishSpawner.Evaluation.Perfect;
			}
			this.StartWait();
		}

		private void StartWait()
		{
			this.fishHitTime = Random.Range(this.fishHitTimeMin, this.fishHitTimeMax);
			this.isWait = true;
		}

		public void OnClickFishingButton()
		{
			this.GetScore(this.evaluation);
			FishSpawner.Instance.Spawn(this.evaluation);
			if(this.fever)
			{
				return;
			}
			if(this.evaluation == FishSpawner.Evaluation.Perfect)
			{
				this.CheckFever();
			}
			this.fisher.PullFloat();
			this.ResetWait();
		}

		private void Hit()
		{
            this.exclamation.SetActive(true);
            this.fishingButton.SetActive(true);
		}

		private void ResetWait()
		{
			this.isWait = false;
			this.waitingTime = 0f;
			this.fishHitTime = 0f;
			this.fishingButton.SetActive(false);
			this.fisher.PullFloat();
            this.exclamation.SetActive(false);
        }

        private void GetScore(FishSpawner.Evaluation evaluation)
		{
            switch (evaluation)
            {
                case (FishSpawner.Evaluation.Good):
                    ScoreService.Instance.AddScore(30);
                    break;
                case (FishSpawner.Evaluation.Great):
                    ScoreService.Instance.AddScore(75);
                    break;
                case (FishSpawner.Evaluation.Perfect):
                    ScoreService.Instance.AddScore(100);
                    break;
                default:
                    ScoreService.Instance.AddScore(5);
                    break;
            }
		}

		private void CheckFever()
		{
			if(Random.Range(0, 10) == 0)
			{
				this.StartFever();
			}
		}

		private void StartFever()
		{
			BgmController.Instance.SetBgm(1);
			this.fever = true;
			this.feverGuage.SetActive(true);
			Invoke("StopFever", 45f);
		}

		private void StopFever()
		{
			this.fever = false;
			this.feverGuage.SetActive(false);
			BgmController.Instance.SetBgm(0);
		}
	}
}