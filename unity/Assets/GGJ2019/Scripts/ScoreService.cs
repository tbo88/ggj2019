﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CFW;

namespace GGJ2019
{
    public enum Difficulty
    {
        Easy,
        Normal,
        Hard
    }

    public class ScoreService : SingletonMonoBehaviour<ScoreService>
    {
        [SerializeField]
        private int maxQuantityEasy = 10;

        [SerializeField]
        private int maxQuantityNormal = 30;

        [SerializeField]
        private int maxQuantityHard = 50;

        [SerializeField]
        private float interval = 5.0f;

        [SerializeField]
        private UnityEngine.UI.Text scoreText;

        [SerializeField]
        private UnityEngine.UI.Text quantityText;

        [SerializeField]
        private GameObject clearAnimation;

        private int maxQuantity = -1;
        private int score;
        private int quantity;
        private float elapsed;

        public void Initialize(Difficulty difficulty)
        {
            switch (difficulty)
            {
                case Difficulty.Easy:
                    this.maxQuantity = maxQuantityEasy;
                    break;
                case Difficulty.Normal:
                    this.maxQuantity = maxQuantityNormal;
                    break;
                case Difficulty.Hard:
                    this.maxQuantity = maxQuantityHard;
                    break;
            }

            this.AddScore(0);
            this.SetQuantity(0);
        }

        private void Update()
        {
            if (this.isPlayed)
            {
                return;
            }
            if (this.maxQuantity < 0)
            {
                return;
            }
            this.elapsed += Time.deltaTime;
            if (this.elapsed < this.interval)
            {
                return;
            }
            this.elapsed -= this.interval;
            var activeFishes = GameObject.FindGameObjectsWithTag("Fish");
            this.SetQuantity(activeFishes.Length);
            if (this.quantity < this.maxQuantity)
            {
                return;
            }
            this.isPlayed = true;
            this.StartCoroutine(this.PlayClearAniamtion());
        }

        private bool isPlayed;

        private IEnumerator PlayClearAniamtion()
        {
            this.clearAnimation.gameObject.SetActive(true);
            yield return new WaitForSeconds(5);
            SceneService.Instance.Move(new Scenes.Ending.Ending.Context());
        }

        public void AddScore(int score)
        {
            this.score += score;
            this.scoreText.text = string.Format("Score:{0}", this.score);
        }

        public void SetQuantity(int quantity)
        {
            this.quantity = quantity;
            this.quantityText.text = string.Format("{0}/{1}", this.quantity, this.maxQuantity);
        }
    }
}
