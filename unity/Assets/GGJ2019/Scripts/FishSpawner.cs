﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using CFW;

namespace GGJ2019
{
    public class FishSpawner : SingletonMonoBehaviour<FishSpawner>
    {
        [System.Serializable]
        public class EvaluationRarityRates
        {
            [System.Serializable]
            public class RarityRatesPair
            {
                public string Rarity;
                public int Rates;
            }
            public Evaluation Evaluation;
            public RarityRatesPair[] rarityRatesPair;
        }

        public enum Evaluation
        {
            Normal,
            Good,
            Great,
            Perfect
        }

        [SerializeField]
        private UnityEngine.UI.Text resultText;

        [SerializeField]
        private Transform spawnPoint;

        [SerializeField]
        private Transform targetPoint;

        [SerializeField]
        private EvaluationRarityRates[] evaluationRarityRatesList;

        [SerializeField]
        private float value;

        public void Spawn(Evaluation evaluation)
        {
            var rarity = this.LotRarity(evaluation);
            var prizes = Services.DataProvider.Instance.GetFishesByRarity(int.Parse(rarity));

            // var index = Random.Range(0, this.rows.Length);
            var index = Random.Range(0, prizes.Count);
            var prize = prizes[index];
            this.resultText.text = prize.Name;
            var fishPrefab = Services.DataProvider.Instance.GetPrefab(prize.ImageId);
            fishPrefab = fishPrefab != null ? fishPrefab : Services.DataProvider.Instance.GetPrefab("can02");
            var fish = GameObject.Instantiate(fishPrefab, this.spawnPoint);

            fish.transform.position = new Vector3(this.GetRandom(), -5f, this.GetRandom() + 20f);
            this.Fishing(fish);

            Services.BookService.Instance.Add(prize.FishId);
        }

        private string LotRarity(Evaluation evaluation)
        {
            var evaluationRarityRates = this.evaluationRarityRatesList.First(element => element.Evaluation == evaluation);
            var sumRates = evaluationRarityRates.rarityRatesPair.Select(element => element.Rates).Sum();
            var result = Random.Range(0, sumRates);
            foreach (var item in evaluationRarityRates.rarityRatesPair)
            {
                result -= item.Rates;
                if (0 > result)
                {
                    return item.Rarity;
                }
            }
            return evaluationRarityRates.rarityRatesPair[0].Rarity;
        }

        private void Fishing(GameObject fish)
        {
            Vector3 velocity = Calculater.CalculateVelocity(
                new Vector3(fish.transform.position.x, fish.transform.position.y, fish.transform.position.z),
                new Vector3(this.targetPoint.position.x, this.targetPoint.position.y, this.targetPoint.position.z),
                70f
            );
            Rigidbody rid = fish.GetComponent<Rigidbody>();
            rid.AddForce(velocity * rid.mass, ForceMode.Impulse);
            rid.AddTorque(new Vector3(this.GetRandomRange(90), this.GetRandomRange(90), this.GetRandomRange(90)));
        }

        private float GetRandom()
        {
            return Random.Range(-this.value, this.value);
        }

        private float GetRandomRange(float value)
        {
            return Random.Range(-value, value);
        }
    }
}
