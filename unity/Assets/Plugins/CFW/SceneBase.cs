﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CFW
{
	public class SceneBase : MonoBehaviour
	{
		public class SceneContext
		{
			// nop
		}

		public virtual IEnumerator OnSceneLoaded(SceneContext context)
		{
			yield break;
		}

		public virtual void OnSceneAdded()
		{
			// nop
		}

		public virtual IEnumerator OnViewRemoved()
		{
			yield break;
		}
	}
}
