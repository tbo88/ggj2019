﻿using System.Collections;
using System.Collections.Generic;

namespace CFW
{
    public sealed class CSVReader
    {
        public Dictionary<string, object>[] Read(string csv)
        {
            var result = new List<Dictionary<string, object>>();
            var ary = csv.Replace("\r", "").Split('\n');
            var keys = CSVReader.ReadLine(ary[0]);
            for (var i = 1; i < ary.Length; i++)
            {
                var values = CSVReader.ReadLine(ary[i]);
                if (values[0].Length == 0)
                {
                    break;
                }
                var dic = new Dictionary<string, object>();
                for (var j = 0; j < keys.Length; j++)
                {
                    dic[keys[j]] = values[j];
                }
                result.Add(dic);
            }
            return result.ToArray();
        }

        private static string[] ReadLine(string str)
        {
            return str.Split(',');
        }
    }
}
