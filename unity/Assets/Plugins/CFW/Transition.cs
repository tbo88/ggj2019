﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace CFW
{
    public class Transition : MonoBehaviour
    {
        [SerializeField]
        private float fadeTime = 0.5f;

        [SerializeField]
        private Image image;

        public IEnumerator FadeIn()
        {
            var elapsed = 0.0f;
            while (elapsed <= this.fadeTime)
            {
                elapsed += Time.deltaTime;
                var color = this.image.color;
                color.a = Mathf.Lerp(0.0f, 1.0f, elapsed / this.fadeTime);
                this.image.color = color;
                yield return null;
            }
        }

        public IEnumerator FadeOut()
        {
            var elapsed = 0.0f;
            while (elapsed <= this.fadeTime)
            {
                elapsed += Time.deltaTime;
                var color = this.image.color;
                color.a = Mathf.Lerp(1.0f, 0.0f, elapsed / this.fadeTime);
                this.image.color = color;
                yield return null;
            }
        }
    }
}
