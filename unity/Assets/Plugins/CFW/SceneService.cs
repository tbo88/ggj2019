﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CFW
{
    public class SceneService : SingletonMonoBehaviour<SceneService>
    {
        [SerializeField]
        private Canvas canvas;

        [SerializeField]
        private Transition transitionPrefab;

        private string currentSceneName;
        private int currentOrder;

        public void Move(SceneBase.SceneContext context)
        {
            this.StartCoroutine(this.MoveWait(context));
        }

        public IEnumerator MoveWait(SceneBase.SceneContext context)
        {
            var transition = GameObject.Instantiate(this.transitionPrefab);
            transition.transform.SetParent(this.canvas.transform, worldPositionStays: true);
            transition.transform.localPosition = Vector3.zero;
            yield return transition.FadeIn();

            if (!string.IsNullOrEmpty(this.currentSceneName))
            {
                yield return this.RemoveWait(this.currentSceneName);
            }
            var sceneName = SceneService.GetSceneName(context.GetType());
            this.currentSceneName = sceneName;
            yield return SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneName));
            var go = GameObject.Find(sceneName);
            var scene = go.GetComponent<SceneBase>();
            yield return scene.OnSceneLoaded(context);
            canvas.targetDisplay = 0;

            yield return transition.FadeOut();
            GameObject.Destroy(transition.gameObject);

            scene.OnSceneAdded();
        }

        public void Add(SceneBase.SceneContext context)
        {
            this.StartCoroutine(this.AddWait(context));
        }

        public IEnumerator AddWait(SceneBase.SceneContext context)
        {
            var sceneName = SceneService.GetSceneName(context.GetType());
            yield return SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            var go = GameObject.Find(sceneName);
            var canvas = go.GetComponentInChildren<Canvas>();
            canvas.targetDisplay = 1;
            this.currentOrder += 100;
            canvas.sortingOrder += this.currentOrder;
            var scene = go.GetComponent<SceneBase>();
            yield return scene.OnSceneLoaded(context);
            canvas.targetDisplay = 0;
            scene.OnSceneAdded();
        }

        public void Remove(SceneBase scene)
        {
            this.StartCoroutine(this.RemoveWait(scene));
        }

        public IEnumerator RemoveWait(string sceneName)
        {
            var go = GameObject.Find(sceneName);
            var scene = go.GetComponent<SceneBase>();
            yield return RemoveWait(scene);
        }

        public IEnumerator RemoveWait(SceneBase scene)
        {
            yield return scene.OnViewRemoved();
            var sceneName = SceneService.GetSceneName(scene.GetType());
            yield return SceneManager.UnloadSceneAsync(sceneName);
            this.currentOrder -= 100;
        }

        private static string GetSceneName(System.Type type)
        {
            var typeName = type.ToString();
            var sceneName = typeName.Substring(typeName.LastIndexOf('.') + 1);
            if (sceneName.IndexOf('+') > 0)
            {
                sceneName = sceneName.Substring(0, sceneName.IndexOf('+'));
            }
            return sceneName;
        }
    }
}
